import random

# Creating the base equipment class
class Equipment:
    def __init__(self, name, slot, value):
        self.name = name
        self.slot = slot
        self.value = value
        if value < 4 and 0:
            min = value - 1
            max = value + 1
        else:
            min = round(value * .75)
            max = round(value * 1.25)
        self.value = int(random.randint(min, max))
        if slot == "wep":
            self.power = self.value
        elif slot == "shield":
            self.defense = self.value
        elif self.slot == "armor":
            self.defense = value
        elif slot == "footwear":
            self.speed = self.value


#  WEAPONS
no_wep = Equipment("none", "wep", 0)
brawler_gloves = Equipment("brawler gloves", "wep", 2)
dagger = Equipment("dagger", "wep", 4)
sword = Equipment("sword", "wep", 7)
spear = Equipment("spear", "wep", 10)
great_axe = Equipment("great axe", "wep", 15)

# Shields
no_shield = Equipment("none", "shield", 0)
wooden_shield = Equipment("wooden shield", "shield", 2)
copper_shield = Equipment("copper shield", "shield", 4)
bronze_shield = Equipment("bronze shield", "shield", 6)
steel_shield = Equipment("steel shield", "shield", 8)

# Armor
no_armor = Equipment("none", "armor", 0)
torn_shirt = Equipment("torn shirt", "armor", 3)
padded_shirt = Equipment("padded shirt", "armor", 5)
leather_tunic = Equipment("leather tunic", "armor", 10),
bronze_tunic = Equipment("bronze tunic", "armor", 20)
steel_suit = Equipment("steel suit", "armor", 35)

# Footwear
no_footwear = Equipment("none", "footwear", 0)
torn_sandals = Equipment("torn sandals", "footwear", 2)
leather_sandals = Equipment("leather sandals", "footwear", 5)
leather_boots = Equipment("leather boots", "footwear", 8)
enhanced_boots = Equipment("enhanced boots", "footwear", 12)
