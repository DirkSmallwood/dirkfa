# Houses all monsters
import random


def randomize(value):
    if value < 4 != 0:
        min = value - 1
        max = value + 1
    else:
        min = round(value * .75)
        max = round(value * 1.25)
    value = abs(int(random.randint(min, max)))
    return value


# Creating the base monster class
class Monster:
    pass

#                #
#    Monsters    #
#                #
class Rat(Monster):
    def __init__(self):
        super().__init__()
        self.name = "rat"
        self.hp = randomize(4)
        self.power = randomize(2)
        self.speed = randomize(5)
        self.defense = randomize(1)


class Skeleton(Monster):
    def __init__(self):
        super().__init__()
        self.name = "skeleton"
        self.hp = randomize(6)
        self.power = randomize(3)
        self.speed = randomize(2)
        self.defense = randomize(1)


class Zombie(Monster):
    def __init__(self):
        super().__init__()
        self.name = "zombie"
        self.hp = randomize(10)
        self.power = randomize(5)
        self.speed = randomize(3)
        self.defense = randomize(1)