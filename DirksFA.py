# Main file the game itself should go here

import random
import Monsters
import Equipment
import time
import sys
from os import environ
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
import pygame


# background music
def play_music(file):
    pygame.mixer.init()
    pygame.mixer.music.load(file)
    pygame.mixer.music.play(-1)

# will be used for sound effects
def play_sound(file):
    return

# randomize used for picking a number between 75% and 125% of a value provided
# if the value is below 4 it should instead pick a number +1 or -1 of the value provided
# if 0 it should stay 0
# this function is used frequently throughout the code for randomizing stats, such as in Dirk() Equipment() Monsters()
def randomize(value):
    if value > 0 < 4:
        min = value - 1
        max = value + 1
    else:
        min = round(value * .75)
        max = round(value * 1.25)
    value = int(random.randint(min, max))
    return value


# Main character class - should probably be reworked to be a main mobile class so monsters could just create from the same class, leaving as is for now
class Dirk:
    def __init__(self):
        self.wep = Equipment.no_wep
        self.shield = Equipment.no_shield
        self.armor = Equipment.no_armor
        self.footwear = Equipment.no_footwear

        self.maxhp = 0
        self.hp = 0
        self.power = 0
        self.speed = 0
        self.defense = 0

        self.potions = 0


# Creating dirk - setting initial stats and equipment
dirk = Dirk()
dirk.maxhp = randomize(20)
dirk.hp = dirk.maxhp
dirk.power = randomize(3)
dirk.speed = randomize(3)

# equipment change handling
def equipchange(item):
        item = item
        if item.slot == "wep":
            dirk.power -= dirk.wep.power
            dirk.wep = item
            dirk.power += item.power
        elif item.slot == "shield":
            dirk.defense -= dirk.shield.defense
            dirk.shield = item
            dirk.defense += item.defense
        elif item.slot == "armor":
            dirk.defense -= dirk.armor.defense
            dirk.armor = item
            dirk.defense += item.defense
        elif item.slot == "footwear":
            dirk.speed -= dirk.footwear.speed
            dirk.footwear = item
            dirk.speed += item.speed


# print the players current stats
def showstats():
    print(""" 
        Stats
   Health: {0} / {1}
    Power: {2}
    Speed: {3} 
    Defense: {4}""".format(dirk.hp, dirk.maxhp, dirk.power, dirk.speed, dirk.defense))


# print the players current equipment & inventory - inventory currently only includes potions
def showequip():
    print("""
    Currently Equipped 
      Weapon: {0}
      Shield: {1}
       Armor: {2}
    Footwear: {3}""".format(str(dirk.wep.name), str(dirk.shield.name), str(dirk.armor.name), str(dirk.footwear.name)))
    print(("\n    Potions: {0}".format(dirk.potions)))


# handling death - if players hp falls to 0 or below, provide and option to restart the current battle and heal, start over completely, or quit
def dead():
    print("You have died, what would you like to do?")
    print("""   
    [1] Continue
    [2] Quit
    [3] Start from beginning""")
    while True:
        try:
            a = int(input())
            break
        except:
            print("Invalid selection. Only enter numbers")
    if a == 1:
        dirk.hp = dirk.maxhp
    elif a == 2:
        exit()
    elif a == 3:
        main()


    if a == 1:
        dirk.hp = dirk.maxhp

    main()

# battle handling
def battle(*args):
    play_music('The Last Encounter Short Loop.wav')
    enemies = []

    for arg in args:
        enemies.append(arg)

    while True:

        print("\n{0}{1:>15}:{2:>3}/{3:<3}  {0}\n{0}{0:>26}""".format("|", "Dirk", dirk.hp, dirk.maxhp))
        print("{0}{1:>15} {2:>3}      {0}".format("|", "Enemy", "hp"))
        a = 0
        for arg in enemies:
            print("{0}     {1:>10}:{2:>3}      {0}".format("|", enemies[a].name, enemies[a].hp))
            a += 1

        print("{0}{0:>26}\n{0}  [1]Attack [2]Potion:{1}  {0}".format("|", dirk.potions))
    #[{2}]


        while True:
            try:
                action = int(input())
            except ValueError:
                print("Not a valid selction, try again")
                continue
            break
        if action == 1:
            print("\n     Who will you attack?")
            a = 0
            for arg in enemies:
                #        print(args[a].name)
                print("{0}     [{2}]{1:>10}:{3:>3}   {0}".format("|", enemies[a].name, a + 1, enemies[a].hp), )
                a += 1
            while True:
                try:
                    attacked = int(input()) - 1
                    target = enemies[attacked]
                    damage_enemy = abs(dirk.power - target.defense)
                    target.hp -= damage_enemy
                    print("you hit", target.name, "for {} damage".format(damage_enemy))
                except:
                    print("Not a valid target, try again")
                    continue
                break
        elif action == 2:
            if dirk.potions == 0:
                print("You are out of potions!")
                continue
            elif dirk.potions > 0:
                amount = round(dirk.maxhp * 0.5)
                dirk.potions -= 1
                dirk.hp += amount
                if dirk.hp > dirk.maxhp:
                    dirk.hp = dirk.maxhp
                print(amount)
                print("You heal for {}".format(amount))
        if target.hp <= 0 and target in enemies:
            print("You have defeated {}".format(target.name))
            enemies.remove(target)
        if len(enemies) == 0:
            print("you have won the battle!")
            break
        else:
            a = 0
            for enemy in enemies:
                damage_dirk = abs(dirk.defense - enemies[a].power)
                dirk.hp -= damage_dirk
                print("{} hits you for {} damage".format(enemies[a].name, damage_dirk))
                a += 1
        if dirk.hp <= 0:
            dead()


# used to print 1 character at a time for storytelling and character conversation
def printout(text):
    text = text
    for char in text:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(.045)
    time.sleep(3)



# the game itself
def main():







    dirk.potions = 2


# Opening story
    print("Dirk's First Adventure")


#     play_music("prologue.mp3")
#     time.sleep(2)
#
#     printout(""""Dirk!" his mother called out as he ran out passed the fence of his family's pasture.
# Dirk saw something moving in the brush and the 8 year old went to investigate, frequently dealing with critters after the livestock.
# He saw a rat that seemed as big as a dog, feeding on an escaped lamb, He turned to tell his mother and was frozen by what he saw.
# His mother lay slumped over face first on the ground, a rusty sword protruding from her back.
# Standing over her was a creature couldn't believe, it appeared to be a human skeleton.
# "I have to find Dad!" he thought as he ran with all his might.
# The skeleton seemed to have no interest in him, as though it had only one purpose, which it had accomplished.
# It turned slowly turned and walked towards the brush where the rat was.
#
# """)
    equipchange(Equipment.brawler_gloves)
    equipchange(Equipment.torn_shirt)
    equipchange(Equipment.torn_sandals)
    showstats()
    showequip()
    battle(Monsters.Rat(), Monsters.Skeleton(), Monsters.Zombie())
    battle(Monsters.Rat())


if __name__ == '__main__':
    main()